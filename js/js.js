$(function(){ // document ready

    $('#bigtext').bigtext({
       // maxfontsize: 60
    });

    window.addEventListener("scroll", function(event) {
    var top = this.scrollY,
        left =this.scrollX;
    
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight
        
        
        if ( top > x ){
            $('[js="backTop"]').show("fast");
        } else{
            $('[js="backTop"]').hide("fast");
        }
    }, false);


    $('[js="backTop"]').on('touch click', function(){
        $('body, html').animate({scrollTop:"0"}, "slow");
    });
   
    $('#bigtext h1 span').prop('Counter',0).animate({
        Counter: $('#bigtext h1 span').text()
    }, {
        duration: 10000,
        easing: 'easeInCirc',
        //easing: 'swing',
        step: function (now) {
            $('#bigtext h1 span').text(Math.ceil(now));
        }
    });

    $(window).load( function(){
        //console.log("go away")
        $('.loader').animate({"opacity":0}, "slow", function(){
          $(this).hide('fast');
        });

        $('section.scroll section.lista .donna').each(function(index){

          back_img = $('.donna-image',this).attr("data-back");
          //$('.donna-image',this).attr("style","background-image:url(" + back_img + ")" );

        });

        $("img.lazy").lazyload({
          failure_limit : 100
        });

        
    });

    var descrizioni = [];
    var total_number = 0;
    var position =  0;
    posHtml = $(".contatore strong");
    risRicerca = $('.risRicerca');

    //$('.fitText').bigtext();

    var options = {
        valueNames: [
            'donna-titolo',
            { data: ['id', 'testo', 'nome', 'anno', 'femminicidio'] }
        ]
    };

    var donneLista = new List('listaDonne', options );
    var activeFilters = [];




function showDescription(lista, index, totale){
    //console.log(lista, index, totale);
    $('.donneList li').removeClass('active');
    $('.donneList li[data-id="' + lista[index] + '"]').addClass('active');
    $("img.lazy").lazyload();

    if (totale == '0') {
        posHtml.html( ( index ) + " di " + totale);
        position = index;
    } else {
        posHtml.html( ( index+1 ) + " di " + totale);
        position = index;
    }
}

//filters


$('#filter_femminicidio').click(function() {
    $('.filtro button').not(this).removeClass('btn-primary').addClass('btn-dafault');
    $('.filtro select').prop('selectedIndex',0);
    $('.sf').css('color','#333');

    if ($(this).hasClass('all')) {
        $(this).removeClass('all').addClass('selected');
        $(this).removeClass('btn-dafault').addClass('btn-primary');
        $('.sf').css('color','#c52b2d');        
        donneLista.filter(function(item) {
            if (item.values().femminicidio == "femminicidio") {
              return true;
            } else {
              return false;
            }
          });
    } else
        {
            $(this).removeClass('selected').addClass('all');
            $(this).removeClass('btn-primary').addClass('btn-dafault');
            donneLista.filter(); // Remove all filters
        }

  return false;
});

    $('#filter_anno').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().anno == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });

    $('#filter_decennio').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().decennio == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });

    $('#filter_statocivile').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().statocivile == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });

    $('#filter_continente').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().continente == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });



    donneLista.on('updated', function(elem){
        //console.log(elem.visibleItems);
        total_number =  elem.matchingItems.length;
        posHtml.html("0 di " + total_number);
        risRicerca.html("<strong>" + total_number + "</strong> risultati");
        console.log( total_number );

        $("img.lazy").lazyload();
        if( 0 == elem.matchingItems.length ){
            posHtml.html("nessun elemento");
            risRicerca.html("nessun elemento");
            $('.contatore').css('display','none');
            $('.messages').html("<p class='notFound'>Spiacente, la ricerca non ha prodotto nessun risultato.</p>");
            descrizioni = [];
            position = 0;

        } else {
             $('.contatore').css('display','block');
            $('.messages').html("");
            descrizioni = [];
            position = 0;
            $.each(elem.matchingItems, function(k, v){
                descrizioni.push( v._values.id );
            });


        }

        showDescription(descrizioni, position, total_number);

    });

    donneLista.update();

    $('.contatore button').on('click', function(){

            if ( $(this).hasClass('prev') ){
                position--;
                if(position<0) { position = descrizioni.length-1; }
            } else if ( $(this).hasClass('next') ){
                position++;
                if(position>descrizioni.length-1) { position = 0; }
            }
            showDescription(descrizioni, position, total_number);
        });

    $('.donneList').on("swipeleft", function(){
        position++;
        if(position>descrizioni.length-1) { position = 0; }
        showDescription(descrizioni, position, total_number);
    });

    $('.donneList').on("swiperight", function(){
        console.log("here");
        position--;
        if(position<0) { position = descrizioni.length-1; }
        showDescription(descrizioni, position, total_number);
    });

    $('#listaDonne ul').on("mouseleave touchmove", function(){
        // console.log("enter");
        $('.donna-image').css('opacity',1);
    })

    $('#listaDonne li').on("touchstart mouseenter", function(){
        // console.log("enter");
        $(this).addClass('tool_tip');
        $('.donna-image').css('opacity',0.7);
        $('.donna-image',this).css('opacity',1);

    }).on("mouseleave touchmove", function(){
        // console.log("leave");
        $(this).removeClass('tool_tip');
    }).on('click', function(){
        // console.log("click");
        $(this).removeClass('tool_tip');
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
    });
    $('a.linke').on('click', function(){
        // console.log("click");
        //$(this).removeClass('tool_tip');
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
    });
    $('a.credits').on('click', function(){
        $("html, body").animate({ scrollTop: $('#crediti').offset().top }, 1000);
    });
});

/*
function showDescription(lista, index, totale){
    //console.log(lista, index, totale);
    $('.donneList li').removeClass('active');
    $('.donneList li[data-id="' + lista[index] + '"]').addClass('active');

    if (totale == '0') {
        posHtml.html( ( index ) + " di " + totale);
    } else {
        posHtml.html( ( index+1 ) + " di " + totale);
    }
}
*/
