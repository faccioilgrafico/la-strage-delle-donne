var handlebars = require('handlebars');
var fs = require('fs');
var moment = require('moment');
var merge = require('merge'), original, cloned;
var lupus = require('lupus');
var http = require('http');
var request = require('request')
var json_merger = require('json_merger');
var objectMerge = require('object-merge');
var absorb = require('absorb');
var iconv = require('iconv-lite');

var elenco = {};
/**
 * Handle multiple requests at once
 * @param urls [array]
 * @param callback [function]
 * @requires request module for node ( https://github.com/mikeal/request )
 */
var __request = function (urls, callback) {

  'use strict';

  var results = {}, t = urls.length, c = 0, requestOptions  = { encoding: null },
    handler = function (error, response, body) {

      var url = response.request.uri.href;
      // occhio alla codifica, qui iconv trasforma la risposta in iso-8859-1
      results[url] = { encoding: null, error: error, response: response, body: iconv.decode(new Buffer(body), "ISO-8859-1") };

      if (++c === urls.length) { callback(results); }

    };
  // occhio alla codifica, importante definire la richiesta con encoding:null
  while (t--) { request({ encoding: null, method: "GET", uri: urls[t]}, handler); }

};

var calculateYearsBetween = function(startDate, endDate) {
    var startYear = startDate.getYear() + 1900,
        endYear = endDate.getYear() + 1900,
        output = []

    for ( var year = startYear; year <= endYear; year++ ) output.push(year)

    return output
}

var years = calculateYearsBetween( new Date("2012-01-01"), new Date() );
// var years = moment().diff('2012-01-01', 'years');
console.log('years')
console.log(years)

prepathjson = 'https://www.corriere.it/27esimaora/strage-delle-donne/';
// prepathjson = 'http://local.corriere.it/cronache/la-strage-delle-donne/data/';
postpathjson = '.json';

var urls = [];
tuttelemiedonne = {};
filename = "tuttelemiedonne.json";

fs.writeFileSync(filename, "");

// for (i = 0; i<years.length; i++) {
for (i=years.length;  i--; i > 0) {

  qualeanno = years[i];
  console.log('qualeanno: ', qualeanno)
  urljson = prepathjson+'schede'+qualeanno+postpathjson+'?ie='+(new Date()).getTime();
  console.log('urljson: ', urljson)
  urls.push(urljson)
};
  console.log(urls)

__request(urls, function(responses) {

  var url, response, tutteinunjson;
  var completed_requests = 0;

  tutteinunjson = [];
  lemiedonne = [];

      for (y = 0; y<urls.length; y++) {

        tutteinunjson = JSON.parse(responses[urls[y]].body);
        tutteinunjson.scheda.forEach(function(scheda) {
          lemiedonne.push(scheda)
        });

      };
      // tutteinunjson = objectMerge(tutteinunjson, JSON.parse(responses[urls[0]].body));
            console.log(lemiedonne[226].nome);
            console.log(lemiedonne[226].data);
            console.log(lemiedonne[226].descrizione);

      jsonOutput = JSON.stringify(lemiedonne, null, 2);
      // jsonOutput = iconv.encode( JSON.stringify(lemiedonne, null, 2) , "UTF-8");
      fs.appendFileSync(filename, jsonOutput);
      // fs.appendFileSync(filename, responses[urls[0]].body.scheda );

      fs.readFile('tuttelemiedonne.json', function(err, datajson){
        if (!err) {
         tuttelemiedonne = JSON.parse(datajson);
         // var tuttelemiedonne = {"scheda": [{"nome":"Rosaria Lentini","luogo":"Caserta"},{"nome":"Vania Vannucchi","luogo":"Lucca"}]};
            // console.log('tuttelemiedonne');
            console.log(lemiedonne.length);
            console.log(lemiedonne[300].nome);
            console.log(lemiedonne[300].data);
            console.log(lemiedonne[5].nome);
            console.log(lemiedonne[5].data);
            console.log(lemiedonne[198].nome);
            console.log(lemiedonne[198].data);

          lemiedonne.forEach(function(elem) {
              if(elenco['donne'] == undefined)
                  elenco['donne'] = [];
              elenco['donne'].push( elem );
          });

          faiPartireTuttoTutto();

        } else {
          // handle file read error
        }
      });

});

function faiPartireTuttoTutto() {


    var url = 'http://corriere.s3.amazonaws.com/json-table/la-strage-delle-donne.json';
    url+="?ie="+(new Date()).getTime();
    var fooJson = {};

    request({
        headers: {
          'User-Agent': 'request',
          'Cache-Control': 'private, no-cache, no-store, must-revalidate',
          'Expires': '-1',
          'Pragma': 'no-cache'
        },
        url: url,
        json: true
    }, function (error, response, body) {
    // test del push to deploy su ftp
        if (!error && response.statusCode === 200) {
            // console.log(body) // Print the json response
            fooJson = body;

            //qui mi gestisco il file
            var donne_leader = {};
            var testo = {};
            var crediti = {};
            var head = {};

            body.testi.forEach(function(testi) {
                for (var key in testi) {

                   if ( testo[key] == undefined )
                        testo[key] = [];
                    if( null != testi[key] ){
                        testo[key].push( testi[key] );
                    }
                   // console.log(key + ": " + testi[key]);
                }
            });
            //console.log(donne_leader_testi);

            body.credits.forEach(function(credits) {
                for (var key in credits) {

                   if ( crediti[key] == undefined )
                        crediti[key] = [];
                    if( null != credits[key] ){
                        crediti[key].push( credits[key] );
                    }
                   // console.log(key + ": " + testi[key]);
                }
            });
            //console.log(donne_leader_credits);

            body.menu.forEach(function(elem) {
                if(elenco['menu'] == undefined)
                    elenco['menu'] = [];
                elenco['menu'].push( elem );
            });
            // console.log("elenco['menu']");
            // console.log(elenco['menu']);

            body.social.forEach(function(elem) {
                if(head['social'] == undefined)
                    head['social'] = [];
                head['social'].push( elem );
            });
            //console.log(donne_leader);

            //poi chiamo la scrittura su file

            /* helpers qui! */
            handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });

            handlebars.registerHelper("normalizzami", function(input) {
              if ( input == "" || input == undefined || input == null ){
                output = "";
                return output;
              } else {
                // output = html_encode(input);
                input = input.replace(/[^\w ]/g, function(char) {
                  return dict2[char] || char;
                });
                return input;
              }
            });

            handlebars.registerHelper("noSpace", function(input) {
              if ( input == "" || input == undefined || input == null ){
                output = "";
                return output;
              } else {
                // output = html_encode(input);
                input = input.replace(/'/g, "\-").replace(/\s+/g, '').replace(/[^\w ]/g, function(char) {
                  return dict2[char] || char;
                });
                return input;
              }
            });



            // read the file and use the callback to render

            fs.readFile('templates/testi.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, testo, "testi.html");
              } else {
                // handle file read error
              }
            });

            fs.readFile('templates/cover.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, elenco, "cover.html");
              } else {
                // handle file read error
              }
            });

            fs.readFile('templates/vittime.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, elenco, "vittime.html");
              } else {
                // handle file read error
              }
            });
            fs.readFile('templates/filtri.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, elenco, "filtri.html");
              } else {
                // handle file read error
              }
            });
            fs.readFile('templates/credits.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, crediti, "credits.html");
              } else {
                // handle file read error
              }
            });


            fs.readFile('templates/elenco.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                // console.log("elenco");
                // console.log(elenco);
                renderToString(source, elenco, "elenco.html");
              } else {
                // handle file read error
              }
            });

            fs.readFile('templates/head.hbs', function(err, data){
                if (!err) {
                // make the buffer into a string
                var source = data.toString();
                // call the render function
                renderToString(source, head, "head.inc");
              } else {
                // handle file read error
              }
            });

            // this will be called after the file is read
            function renderToString(source, data, filename) {
              var template = handlebars.compile(source);
              var outputString = template(data);
              outputString = iconv.encode(outputString, "ISO-8859-1");
              apri_hanldetemple = '';
              chiudi_hanldetemple = '';

              fs.writeFileSync(filename, "");
              fs.appendFileSync(filename, apri_hanldetemple);
              fs.appendFileSync(filename, outputString);
              fs.appendFileSync(filename, chiudi_hanldetemple);
              // console.log(outputString);
              // return outputString;
            }


        } else {

        }
    });

};

function sortObject(obj, testVal) {
    var arr = [];
    var prop;

    for (prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop][testVal]
            });
        }
    }

    arr.sort(function(a, b) {
        if (a.value > b.value) {
          return 1;
        }
        if (a.value < b.value) {
          return -1;
        }
        // a must be equal to b
        return 0;
    });


    return arr; // returns array
}


var dict2 = {
"’":"'",
"À":"&Agrave;",
"Á":"&Aacute;",
"Â":"&Acirc;",
"Ã":"&Atilde;",
"Ä":"&Auml;",
"Å":"&Aring;",
"Æ":"&AElig;",
"Ç":"&Ccedil;",
"È":"&Egrave;",
"É":"&Eacute;",
"Ê":"&Ecirc;",
"Ë":"&Euml;",
"Ì":"&Igrave;",
"Í":"&Iacute;",
"Î":"&Icirc;",
"Ï":"&Iuml;",
"Ð":"&ETH;",
"Ñ":"&Ntilde;",
"Ò":"&Ograve;",
"Ó":"&Oacute;",
"Ô":"&Ocirc;",
"Õ":"&Otilde;",
"Ö":"&Ouml;",
"×":"&times;",
"Ø":"&Oslash;",
"Ù":"&Ugrave;",
"Ú":"&Uacute;",
"Û":"&Ucirc;",
"Ü":"&Uuml;",
"Ý":"&Yacute;",
"Þ":"&THORN;",
"ß":"&szlig;",
"à":"&agrave;",
"á":"&aacute;",
"â":"&acirc;",
"ã":"&atilde;",
"ä":"&auml;",
"å":"&aring;",
"æ":"&aelig;",
"ç":"&ccedil;",
"è":"&egrave;",
"é":"&eacute;",
"ê":"&ecirc;",
"ë":"&euml;",
"ì":"&igrave;",
"í":"&iacute;",
"î":"&icirc;",
"ï":"&iuml;",
"ð":"&eth;",
"ñ":"&ntilde;",
"ò":"&ograve;",
"ó":"&oacute;",
"ô":"&ocirc;",
"õ":"&otilde;",
"ö":"&ouml;",
"÷":"&divide;",
"ø":"&oslash;",
"ù":"&ugrave;",
"ú":"&uacute;",
"û":"&ucirc;",
"ü":"&uuml;",
"ý":"&yacute;",
"þ":"&thorn;",
"ÿ":"&yuml;",
"¡":"&iexcl;",
"¢":"&cent;",
"£":"&pound;",
"¤":"&curren;",
"¥":"&yen;",
"¦":"&brvbar;",
"§":"&sect;",
"¨":"&uml;",
"©":"&copy;",
"ª":"&ordf;",
"«":"&laquo;",
"¬":"&not;",
"®":"&reg;",
"¯":"&macr;",
"°":"&deg;",
"±":"&plusmn;",
"²":"&sup2;",
"³":"&sup3;",
"´":"&acute;",
"µ":"&micro;",
"¶":"&para;",
"·":"&middot;",
"¸":"&cedil;",
"¹":"&sup1;",
"º":"&ordm;",
"»":"&raquo;",
"¼":"&frac14;",
"½":"&frac12;",
"¾":"&frac34;",
"€":"&euro;",
"‚":"&sbquo;",
"ƒ":"&fnof;",
"„":"&bdquo;",
"…":"&hellip;",
"†":"&dagger;",
"‡":"&Dagger;",
"ˆ":"&circ;",
"‰":"&permil;",
"Š":"&Scaron;",
"‹":"&lsaquo;",
"Œ":"&OElig;",
"Ž":"&Zcaron;",
"‘":"&lsquo;",
"’":"&rsquo;",
"“":"&ldquo;",
"”":"&rdquo;",
"•":"&bull;",
"–":"&ndash;",
"—":"&mdash;",
"˜":"&tilde;",
"™":"&trade;",
"š":"&scaron;",
"›":"&rsaquo;",
"œ":"&oelig;",
"ž":"&zcaron;",
"Ÿ":"&Yuml;",
"Ā":"&Amacr;",
"ā":"&amacr;",
"Ă":"&Abreve;",
"ă":"&abreve;",
"Ą":"&Aogon;",
"ą":"&aogon;",
"Ć":"&Cacute;",
"ć":"&cacute;",
"Ĉ":"&Ccirc;",
"ĉ":"&ccirc;",
"Ċ":"&Cdod;",
"ċ":"&cdot;",
"Č":"&Ccaron;",
"č":"&ccaron;",
"Ď":"&Dcaron;",
"ď":"&dcaron;",
"Đ":"&Dstrok;",
"đ":"&dstrok;",
"Ē":"&Emacr;",
"ē":"&emacr;",
"Ė":"&Edot;",
"ė":"&edot;",
"Ę":"&Eogon;",
"ę":"&eogon;",
"Ě":"&Ecaron;",
"ě":"&ecaron;",
"Ĝ":"&Gcirc;",
"ĝ":"&gcirc;",
"Ğ":"&Gbreve;",
"ğ":"&gbreve;",
"Ġ":"&Gdot;",
"ġ":"&gdot;",
"Ģ":"&Gcedil;",
"ģ":"&gcedil;",
"Ĥ":"&Hcirc;",
"ĥ":"&hcirc;",
"Ħ":"&Hstrok;",
"ħ":"&hstrok;",
"Ĩ":"&Itilde;",
"ĩ":"&itilde;",
"Ī":"&Imacr;",
"ī":"&imacr;",
"Į":"&Iogon;",
"į":"&iogon;",
"İ":"&Idot;",
"ı":"&inodot;",
"Ĳ":"&IJlog;",
"ĳ":"&ijlig;",
"Ĵ":"&Jcirc;",
"ĵ":"&jcirc;",
"Ķ":"&Kcedil;",
"ķ":"&kcedil;",
"ĸ":"&kgreen;",
"Ĺ":"&Lacute;",
"ĺ":"&lacute;",
"Ļ":"&Lcedil;",
"ļ":"&lcedil;",
"Ľ":"&Lcaron;",
"ľ":"&lcaron;",
"Ŀ":"&Lmodot;",
"ŀ":"&lmidot;",
"Ł":"&Lstrok;",
"ł":"&lstrok;",
"Ń":"&Nacute;",
"ń":"&nacute;",
"Ņ":"&Ncedil;",
"ņ":"&ncedil;",
"Ň":"&Ncaron;",
"ň":"&ncaron;",
"ŉ":"&napos;",
"Ŋ":"&ENG;",
"ŋ":"&eng;",
"Ō":"&Omacr;",
"ō":"&omacr;",
"Ő":"&Odblac;",
"ő":"&odblac;",
"Œ":"&OElig;",
"œ":"&oelig;",
"Ŕ":"&Racute;",
"ŕ":"&racute;",
"Ŗ":"&Rcedil;",
"ŗ":"&rcedil;",
"Ř":"&Rcaron;",
"ř":"&rcaron;",
"Ś":"&Sacute;",
"ś":"&sacute;",
"Ŝ":"&Scirc;",
"ŝ":"&scirc;",
"Ş":"&Scedil;",
"ş":"&scedil;",
"Š":"&Scaron;",
"š":"&scaron;",
"Ţ":"&Tcedil;",
"ţ":"&tcedil;",
"Ť":"&Tcaron;",
"ť":"&tcaron;",
"Ŧ":"&Tstrok;",
"ŧ":"&tstrok;",
"Ũ":"&Utilde;",
"ũ":"&utilde;",
"Ū":"&Umacr;",
"ū":"&umacr;",
"Ŭ":"&Ubreve;",
"ŭ":"&ubreve;",
"Ů":"&Uring;",
"ů":"&uring;",
"Ű":"&Udblac;",
"ű":"&udblac;",
"Ų":"&Uogon;",
"ų":"&uogon;",
"Ŵ":"&Wcirc;",
"ŵ":"&wcirc;",
"Ŷ":"&Ycirc;",
"ŷ":"&ycirc;",
"Ÿ":"&Yuml;",
"Ź":"&Zacute;",
"ź":"&zacute;",
"Ż":"&Zdot;",
"ż":"&zdot;",
"Ž":"&Zcaron;",
"ž":"&zcaron;",
"¿":"&iquest;"
}
