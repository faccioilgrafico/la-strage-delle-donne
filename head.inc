
    
    <title>Oltre la violenza - Corriere.it</title>
    <meta name="title" content="Oltre la violenza - Corriere.it">
    <meta name="description" content="Uccise. Da mariti, fidanzati, spasimanti... Ma anche vittime di rapinatori o di uomini semplicemente violenti, anche per motivi futili.">
    
    <meta property="fb:app_id" content="203568503078644"/>
    <meta property="og:title" content="Oltre la violenza"/>
    <meta property="og:type" content="article"/>
    <meta property="og:locale" content="it_IT"/>
    <meta property="og:url" itemprop="url" content="http://27esimaora.corriere.it/la-strage-delle-donne"/>
    <meta property="og:image" content="//27esimaora.corriere.it/la-strage-delle-donne/img/social.png"/>
    <meta property="og:site_name" content="Corriere della Sera"/>
    <meta property="og:description" content="Dal 2012 ad oggi <!--#include virtual="vittime.html"--> vittime. Uccise. Da mariti, fidanzati, spasimanti... Ma anche vittime di rapinatori o di uomini semplicemente violenti, anche per motivi futili."/>
    <meta name="author" content="Laura Zangarini"/>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Corriereit">
    <meta name="twitter:creator" content="Laura Zangarini">
    <meta name="twitter:title" content="Oltre la violenza">
    <meta name="twitter:description" content="Dal 2012 ad oggi <!--#include virtual="vittime.html"--> vittime. Uccise. Da mariti, fidanzati, spasimanti... Ma anche vittime di rapinatori o di uomini semplicemente violenti, anche per motivi futili.">
    <meta name="twitter:image" content="//27esimaora.corriere.it/la-strage-delle-donne/img/social.png">

    <link rel="canonical" href="https://27esimaora.corriere.it/la-strage-delle-donne"/>

  <meta property="vr:canonical" content="https://27esimaora.corriere.it/la-strage-delle-donne"/>






